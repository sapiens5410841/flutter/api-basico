import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ShippingPage extends StatefulWidget {
  const ShippingPage({super.key});

  @override
  State<ShippingPage> createState() => _ShippingPageState();
}

class _ShippingPageState extends State<ShippingPage> {
  double _sbh = 30.0;
  TextEditingController _cepController = TextEditingController();
  TextEditingController _ruaController = TextEditingController();
  TextEditingController _bairroController = TextEditingController();
  TextEditingController _complementoController = TextEditingController();
  TextEditingController _cidadeController = TextEditingController();
  TextEditingController _estadoController = TextEditingController();

  void ConsultaCep() async{

    var url = Uri.parse("https://viacep.com.br/ws/${_cepController.text}/json/");
    http.Response response;
    response = await http.get(url);
    
    Map<String, dynamic> retorno = json.decode(response.body);
    _ruaController.text = retorno["logradouro"];
    _bairroController.text = retorno["bairro"];
    _complementoController.text = retorno["complemento"];
    _cidadeController.text = retorno["localidade"];
    _estadoController.text = retorno["uf"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text("Shipping API")),
      body: ListView(children: [Container(child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextField(keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "CEP"),
              controller: _cepController,
            ),
            SizedBox(height: _sbh,),
            TextField(keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Rua"),
              controller: _ruaController,
            ),
            SizedBox(height: _sbh,),
            TextField(keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Bairro"),
              controller: _bairroController,
            ),
            SizedBox(height: _sbh,),
            TextField(keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Complemento"),
              controller: _complementoController,
            ),
            SizedBox(height: _sbh,),
            TextField(keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Cidade"),
              controller: _cidadeController,
            ),
            SizedBox(height: _sbh,),
            TextField(keyboardType: TextInputType.text,
              decoration: InputDecoration(labelText: "Estado"),
              controller: _estadoController,
            ),
            SizedBox(height: _sbh,),
            ElevatedButton(child: Text("Consulta"), onPressed: ConsultaCep)
        ],),
      ),)],),
    );
  }
}
