import 'package:flutter/material.dart';

import 'MoneyPage.dart';
import 'ShippingPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double _sbh = 30.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Operações e API")),
      body: ListView(children: [Container(
        child:Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            GestureDetector(child: Image.asset("images/1.png"),
              onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MoneyPage()));
            }),
            SizedBox(height: _sbh,),
            GestureDetector(child: Image.asset("images/2.png"),
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ShippingPage()));
            },)
          ],),
        ),)],),
    );
  }
}
